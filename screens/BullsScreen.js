import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  AsyncStorage,
  Text,
  TouchableOpacity,
  View,
  FlatList
} from 'react-native';

import { WebBrowser } from 'expo';
import { List, ListItem } from 'react-native-elements'
import { MonoText } from '../components/StyledText';
import RootNavigation from '../navigation/RootNavigation';


export default class BullsScreen extends React.Component {

  // static navigationOptions: (navigation) => {
  //   const { state: { params = {} } } = navigation;
  //   return {
  //     title: navigation.state.params.producer.name || 'default title',
  //   };
  // }

  static navigationOptions = {
    title: "Bulls"
  };

  state = {
    isLoading: true
  };

  constructor() {
    super();

  }

  openSubmissions(bull) {

    // producer_id = this.props.navigation.state.params.producer_id;

    this.props.navigation.navigate('Submissions', {
      bull: bull
    })

  }

  render() {

    var producer = this.props.navigation.state.params.producer;

    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerText}>Select a bull from {producer.name}</Text>
        </View>

        <ScrollView>
          {
            producer.bulls.map((item, i) => (

              <ListItem
                key={i}
                title={item.name}
                onPress={() => this.openSubmissions(item)}
              />

            ))
          }
        </ScrollView>


      </View>
    );
  }

  componentDidMount() {

  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 0
  },
  headerText: {
    color: 'white',
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#123',
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'black'
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    borderBottomColor: 'blue'
  },
})
