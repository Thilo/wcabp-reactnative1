import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  View,
  AsyncStorage
} from 'react-native';

import { WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';

import RootNavigation from '../navigation/RootNavigation';

export default class LoginScreen extends React.Component {
  
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      auth_token: '',
    }
  }

  componentDidMount() {
    this._loadInitialState().done();
  }

  _loadInitialState = async () => {
    var value = await AsyncStorage.getItem('auth_token');
    if (value == '') {
      this.props.navigation.navigate("Login");
    }
  }

  render() {
    return (

      <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>

        <View style={styles.container}>

          <Text style={styles.header}>
            <Image
              source={ require('../assets/images/logo1.png') }

              style={{
                width: 270
              }}

            />
          </Text>

          <TextInput
            style={styles.textInput} placeholder='Username'
            autoCapitalize='none'
            onChangeText={ (username) => this.setState({username})  }
            underlineColorAndroid='transparent'

          />

          <TextInput
            style={styles.textInput} placeholder='Password'
            autoCapitalize='none'
            onChangeText={ (password) => this.setState({password})  }
            underlineColorAndroid='transparent'
            secureTextEntry={true}
          />

          <TouchableOpacity
            style={styles.btn}
            onPress={this.login}>

            <Text style={styles.buttontext}>Login</Text>
          
          </TouchableOpacity>

        </View>

      </KeyboardAvoidingView>


    );
  }

  login = () => {
    
    fetch('https://www.farmops.net/wcabp/api/v3/login.php', {
      method: 'POST',
      header: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
        auth_token: this.state.auth_token,
        hydrated: 1
      })
    })
    .then((response) => response.json())
    .then((res) => {

      if ( ! res.error ) {

        AsyncStorage.setItem('clinic', JSON.stringify(res.data.clinic));
        AsyncStorage.setItem('username', res.data.name);
        AsyncStorage.setItem('auth_token', res.data.auth_token);

        this.props.navigation.navigate("Main");
      }
      else {
        alert(res.message);
      }
    })
    .done();
  }


}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#eee',
    paddingLeft: 40,
    paddingRight: 40,
  },
  header: {
    fontSize: 24,
    marginBottom: 60,
    color: '#416db0',
    fontWeight: 'bold',
  },
  textInput: {
    alignSelf: 'stretch',
    padding: 16,
    marginBottom: 20,
    backgroundColor: '#fff',
  },
  btn: {
    alignSelf: 'stretch',
    backgroundColor: '#416db0',
    padding: 20,
    alignItems: 'center',
  },
  buttontext: {
    color: 'white'
  }
});