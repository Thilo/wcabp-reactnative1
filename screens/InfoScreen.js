import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { Button } from 'react-native-elements'

export default class InfoScreen extends React.Component {
  static navigationOptions = {
    title: 'Info',
  };

  render() {
    return (
      <View style={styles.container}>
        {/* Go ahead and delete ExpoLinksView and replace it with your
           * content, we just wanted to provide you with some helpful links */}

        <View style={styles.buttonPadding}>
          <Button
            raised
            icon={{name: 'assignment'}}
            backgroundColor='#416db0'
            color='white'
            title='INFO FOR PRODUCERS' />
        </View>

        <View style={styles.buttonPadding}>
          <Button
            raised
            icon={{name: 'assignment'}}
            backgroundColor='#416db0'
            color='white'
            title='INFO FOR PRACTITIONERS' />
        </View>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  buttonPadding: {
    paddingBottom: 20
  }
});
