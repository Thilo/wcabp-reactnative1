import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native';

import {
  FormLabel,
  FormInput,
  FormValidationMessage
} from 'react-native-elements'

import Autocomplete from 'react-native-autocomplete-input'


export default class ReportsScreen extends React.Component {
  static navigationOptions = {
    title: 'Reports',
  };

  state = {
    query: ""
  }

  someFunction() {
    alert('checked')
  }


  render() {
    
  
    const { query } = this.state;
    // const { query } = this.state;
    // 
    //const data = this._filterData(query)
    const data = ['something', 'Something', 'Somesing'];

    return (

        <Autocomplete
          data={data}
          defaultValue={query}
          onChangeText={text => this.setState({ query: text })}
          renderItem={item => (
            <TouchableOpacity onPress={() => this.setState({ query: item })}>
              <Text>{item}</Text>
            </TouchableOpacity>
          )}
        />        

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
