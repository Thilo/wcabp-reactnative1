import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  NetInfo,
  StyleSheet,
  Button,
  AsyncStorage,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  FlatList
} from 'react-native';

import { WebBrowser } from 'expo';
import { List, ListItem } from 'react-native-elements'
import { MonoText } from '../components/StyledText';
import RootNavigation from '../navigation/RootNavigation';

import { bindActionCreators } from 'redux'

import ActivityIndicator from 'react-native-activity-indicator'

import { getTest, updateConnectionStatus } from '../actions/status'
import { refreshProducers } from '../actions/producers'

import { connect } from 'react-redux'

export class ProducersScreen extends React.Component {

   static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let headerLeft = null;
    let headerRight = (
      <Button
        title="Refresh"
        onPress={params.refresh ? params.refresh : () => null}
      />
    );
    if (params.isRefreshing) {
      headerRight = <ActivityIndicator />;
    }
    return { headerRight, headerLeft };
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isRefreshing: true,
      queue: []
    };
  }

  openProducer(producer) {

    this.props.navigation.navigate('Bulls', {producer: producer})
  }

  _refresh = () => {

    // Update state, show ActivityIndicator
    this.props.navigation.setParams({ isRefreshing: true });
    
    this.props.actions.refreshProducers();

    this.props.actions.getTest(this._getRandomId())

    this.props.navigation.setParams({ isRefreshing: false});

  }

  _getRandomId() {
    return "item_"+Math.round(Math.random()*1000000000);
  }

  _goOnline() {
    
    this.props.actions.updateConnectionStatus('online');

    var { queue } = this.props.state.status;
    if (queue == undefined) {
      queue = [];
    }
    if (queue.length > 0) {
      queue.forEach(queue_id => {
        this.props.actions.getTest(queue_id, true);
      });
    }
  }
  _goOffline() {
    this.props.actions.updateConnectionStatus('offline');
  }



  // Remove listener when component unmounts
  componentWillUnmount() {
    // NetInfo.isConnected.removeEventListener('change', this.networkConnectionChange);
    
  }

  componentDidMount() {

    // this mocks a network change
    this._goOnline();

    // NetInfo.isConnected.addEventListener('change', this.networkConnectionChange);

    this._loadInitialState().done();

    // We can only set the function after the component has been initialized
    this.props.navigation.setParams({ refresh: this._refresh });
  }

  render() {

    if (this.state.isLoading) {
      return <View><Text>Loading...</Text></View>;
    }

    let online_module = null;
    if (this.props.state.status.isConnected == "online") {
      online_module = <Button onPress={() => this._goOffline()} title='GO OFFLINE' />
    }
    else {
      online_module = <Button onPress={() => this._goOnline()} title='GO ONLINE' />
    }

    let queue_items_header = null;
    let queue_items_footer = null;

    if (this.props.state.status.queue.length > 0) {

      queue_items_header =
        <Text style={styles.headerText}>
          Queueue'd items:
        </Text>;

      queue_items_footer =
        <View style={styles.header}>
          {queue_items_header}
        </View>

    } else {
      queue_items_header =
        <Text style={styles.headerText}>
          Queue is cleared!
        </Text>;      
    }

    console.log(this.props.state);

    return (

      <View style={styles.container}>

        <View style={styles.header}>
          {/*<Text style={styles.headerText}>Version: {this.props.state.test.test}</Text>*/}
          <Text style={styles.headerText}>Last refreshed: {this.props.state.producers.last_updated}</Text>
          <Text style={styles.headerText}>Currently: {this.props.state.status.isConnected}</Text>
          <Text style={styles.headerText}>Producer count: {this.props.state.status.count}</Text>
        </View>

        <View style={styles.buttonPadding}>
          {online_module}
        </View>

        <View style={styles.header}>{queue_items_header}</View>

        <View>
          {
            this.props.state.status.queue.map((item, i) => (
              <ListItem key={i} title={item} />
            ))
          }
        </View>

        {queue_items_footer}

        <ScrollView>
          {
            this.list.map((item, i) => (
              <ListItem key={i} title={item.name} onPress={() => this.openProducer(item)} />
            ))
          }
        </ScrollView>

      </View>

    );
  }

  // Network status change handler
  networkConnectionChange = (isConnected) => {


  };

  _loadInitialState = async () => {

    var value = await AsyncStorage.getItem('auth_token');

    if (value == '' || value == null) {
      this.props.navigation.navigate("Login");
    }

    AsyncStorage.getItem('clinic').then((clinicString) => {

      clinic = JSON.parse(clinicString);

      this.list = clinic.producers

      this.setState({
        isLoading: false
      });
    });
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 0
  },
  topRightButton: {
    padding: 10
  },

  headerText: {
    color: 'white',
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#123',
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'black'
  },

  booya: {
    padding: 10,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    borderBottomColor: 'blue'
  },
})

export default connect(state => ({
    state: state
  }),
  (dispatch) => ({
    actions: { ...bindActionCreators({ getTest, updateConnectionStatus, refreshProducers}, dispatch) }
    // actions: bindActionCreators(actionCreators, dispatch)
  })
)(ProducersScreen);