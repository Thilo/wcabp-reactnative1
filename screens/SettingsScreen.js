import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  NetInfo,
  StyleSheet,
  Button,
  AsyncStorage,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  FlatList
} from 'react-native';

import RootNavigation from '../navigation/RootNavigation';
import { NavigationActions } from 'react-navigation'

import { logOut } from '../actions/producers'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

export class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Settings',
  };

  logout() {

    AsyncStorage.removeItem('auth_token')

    this.props.actions.logOut();

    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Login'})
      ]
    })
    this.props.navigation.dispatch(resetAction)

  }
  
  render() {

    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return(
      <View style={styles.container}>

        <TouchableOpacity
          padding
          style={styles.btn}
          onPress={() => this.logout()}>

          <Text style={styles.btntext}>Log out</Text>
        
        </TouchableOpacity>

    </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 40
  },
  helpLinkContainer: {
    paddingBottom: 40
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  btn: {
    alignSelf: 'stretch',
    backgroundColor: '#416db0',
    padding: 20,
    alignItems: 'center',
  },
  btntext: {
    color: 'white'
  }

})


export default connect(state => ({
    state: state
  }),
  (dispatch) => ({
    actions: { ...bindActionCreators({ logOut }, dispatch) }
    // actions: bindActionCreators(actionCreators, dispatch)
  })
)(SettingsScreen);