import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  AsyncStorage,
  Text,
  TouchableOpacity,
  View,
  FlatList
} from 'react-native';

import { WebBrowser } from 'expo';
import { List, ListItem } from 'react-native-elements'
import { MonoText } from '../components/StyledText';
import RootNavigation from '../navigation/RootNavigation';


export default class SubmissionsScreen extends React.Component {
  static navigationOptions = {
    title: "Submissions"
  };

  state = {
    isLoading: true
  };

  constructor() {
    super();

  }

  openSubmission(submission) {

    this.props.navigation.navigate('Submission', {submission: submission})

  }

  render() {

    var bull = this.props.navigation.state.params.bull;

    var evaluations = this.props.navigation.state.params.bull.evaluations

    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerText}>Select a submission from {bull.name}</Text>
        </View>

        <ScrollView>
          {
            evaluations.map((item, i) => (

              <ListItem
                key={i}
                title={item.evaluation_date}
                onPress={() => this.openSubmission(item)}
              />

            ))
          }
        </ScrollView>


      </View>
    );
  }

  componentDidMount() {


  
  }

}


const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 0
  },
  headerText: {
    color: 'white',
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#123',
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'black'
  },  
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    borderBottomColor: 'blue'
  },
})
