// import { combineReducers } from 'redux';
// // import {reducer as formReducer } from 'redux-form';
// import producerReducer from './producer_reducer';
// import statusReducer from './status_reducer';

// const rootReducer = combineReducers({
//   producers: producerReducer,
//   status: statusReducer,
// })

// export default rootReducer;



import { combineReducers } from 'redux';
// import {reducer as formReducer } from 'redux-form';
// import userReducer from './user_reducer.js';
import producerReducer from './producer_reducer.js';
import statusReducer from './status_reducer.js';

const appReducer = combineReducers({
  producers: producerReducer,
  status: statusReducer,
})

const rootReducer = ( state, action ) => {
  if ( action.type === 'LOG_OUT' ) {
    state = undefined;
  }
      
  return appReducer(state, action)
}

export default rootReducer;