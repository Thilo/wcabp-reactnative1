
import {
  REFRESH_PRODUCERS
} from '../actions/producers'

export default (state = {
  last_updated: "Never",
  producers: []
}, action) => {

  switch (action.type) {
    case REFRESH_PRODUCERS:
      return {...state, last_updated: action.payload}
    default: 
      return state;
  }
}
