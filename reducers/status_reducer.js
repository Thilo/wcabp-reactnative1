import without from 'lodash/without';

import {
  CONNECTION_STATUS,
  ADD_TO_ACTION_QUEUE,
  REMOVE_FROM_ACTION_QUEUE,
  OFFLINE_COUNTER
} from '../actions/status'

export default (state = {
  isConnected: false,
  count: "Refresh to view",
  queue: []
}, action) => {

  switch (action.type) {
    case OFFLINE_COUNTER:
      return {
        ...state, count: action.payload,
      };
    case CONNECTION_STATUS:
      return {
        ...state, isConnected: action.payload,
      };
    case ADD_TO_ACTION_QUEUE:
      return {
        ...state, queue: [...state.queue, action.payload],
      };
    case REMOVE_FROM_ACTION_QUEUE:
      return {
        ...state, queue: without(state.queue, action.payload),
      };
    default: 
      return state;
  }
}