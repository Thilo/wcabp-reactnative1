import { createStore, applyMiddleware, combineReducers } from 'redux';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import thunk from 'redux-thunk';
// import { createLogger } from 'redux-logger';

// import { composeWithDevTools } from 'redux-devtools-extension'
// composeWithDevTools

import { composeWithDevTools } from 'redux-devtools-extension'

const persistConfig = {
  key: 'root',
  storage: storage,
}

import reducers from '../reducers';
// const reducer = combineReducers(reducers);

const persistedReducer = persistReducer(persistConfig, reducers)



// const logger = createLogger();
// const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
//const createStoreWithMiddleware = composeWithDevTools(applyMiddleware(thunk))(createStore);

// const store = applyMiddleware(thunk)(createStore)(persistedReducer);

export default () => {
  let store = applyMiddleware(thunk)(createStore)(persistedReducer);
  let persistor = persistStore(store)
  return { store, persistor }
}