import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../constants/Colors';

import ProducersScreen from '../screens/ProducersScreen';
import InfoScreen from '../screens/InfoScreen';
import ReportsScreen from '../screens/ReportsScreen';
import SettingsScreen from '../screens/SettingsScreen';

import { connect } from 'react-redux'

export default TabNavigator(

  {
    Submissions: {
      screen: ProducersScreen,
    },
    Info: {
      screen: InfoScreen,
    },
    Reports: {
      screen: ReportsScreen,
    },
    Settings: {
      screen: SettingsScreen,
    },

  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Submissions':
            iconName =
              Platform.OS === 'ios'
                ? `ios-paper${focused ? '' : '-outline'}`
                : 'md-paper';
            break;
          case 'Info':
            iconName = Platform.OS === 'ios' ? `ios-information${focused ? '' : '-outline'}` : 'md-information';
            break;
          case 'Reports':
            iconName = Platform.OS === 'ios' ? `ios-link${focused ? '' : '-outline'}` : 'md-link';
            break;
          case 'Settings':
            iconName =
              Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'md-options';
        }
        return (
          <Ionicons
            name={iconName}
            size={28}
            style={{ marginBottom: -3 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);