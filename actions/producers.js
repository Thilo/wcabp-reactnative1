import axios from 'axios'

export const REFRESH_PRODUCERS = 'REFRESH_PRODUCERS'

export const refreshProducers = () => async (dispatch, getState) => {

  const { status } = getState();

  // Make fake API call if user is online 
  if ( status.isConnected == 'online') {

    return axios.get("https://www.farmops.net/wcabp/api/v3/test.php?test=1").then((response =>{
      
      dispatch({ type: REFRESH_PRODUCERS, payload:response.data.data.time });
        
    }))
  }
}

export const logOut = () => async (dispatch, getState) => {

  dispatch({ type: 'LOG_OUT', payload: null });
}