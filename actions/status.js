import axios from 'axios'

export const CONNECTION_STATUS = 'CONNECTION_STATUS'
export const ADD_TO_ACTION_QUEUE = 'ADD_TO_ACTION_QUEUE'
export const REMOVE_FROM_ACTION_QUEUE = 'REMOVE_FROM_ACTION_QUEUE'
export const OFFLINE_COUNTER = 'OFFLINE_COUNTER'

// Change network connection status
export const updateConnectionStatus = (status) => (dispatch, getState) => {

  console.log("You are: "+status);

  dispatch({ type: CONNECTION_STATUS, payload: status });
};

export const clearQueue = (queue_id) => async (dispatch, getState) => {
  
  dispatch({ type: REMOVE_FROM_ACTION_QUEUE, payload: queue_id });
  console.log(queue_id+" CLEARED from queue");
}

export const getTest = (queue_id, queued = false) => async (dispatch, getState) => {

  const { status } = getState();

  // Make fake API call if user is online 
  if ( status.isConnected == 'online') {

    return axios.get("https://www.farmops.net/wcabp/api/v3/counter.php?test=1").then((response =>{
    
      console.log(queue_id+" SENT to server");

      if (queued) {
        console.log(queue_id+" CLEARED from queue");
        dispatch({ type: REMOVE_FROM_ACTION_QUEUE, payload: queue_id });
      }

      dispatch({ type: OFFLINE_COUNTER, payload: response.data.data.count });

    }))
  }
  // Store in the queue if user is offline
  else {
    console.log(queue_id+" ADDED to queue because you're offline");
    dispatch({ type: ADD_TO_ACTION_QUEUE, payload: queue_id });
  }
};

// // Change network connection status
// export const updateConnectionStatus = (status) => (dispatch, getState) => {
//   dispatch({ type: 'CONNECTION_STATUS', isConnected: status });
// };
